/** -- LayoutController
 * @version 0.2
 */

 function LayoutController()
{
    this.layouts = []; // Layout array
    this._current_layout = {};
    this._hasInitialized = false;
}

LayoutController.prototype.initializeLayout = function( layout_name )
{
    var _this = this;
    if (this._hasInitialized === true) { return; }
    _this._hasInitialized = true; // make sure that init is not called many times.
    // First we hide every layout there is and check if the new layout is found.
    _this.layouts.forEach(function(layout)
    {
        layout.hide()
        if (layout.getName === layout_name)
        {
            layout.show() // display the initialized layout
            _this._current_layout = layout; // and saving the current layout.
        }
    });
}

LayoutController.prototype.changeLayout = function( layout_name )
{
    if (this._current_layout)
    {
        this._current_layout.hide()
        var found = this.layouts.find(function(layout)
        {
            return layout.getName === layout_name;
        });
        found.show()
        this._current_layout = found;
    }
    else
    {
        console.error("The layout must be initalized first using initializeLayout(layout_name)");
    }
}

LayoutController.prototype.addLayout = function( layout_name, layout_elements )
{
    var layout = new Layout(layout_name, layout_elements);
    this._current_index ++;
    this.layouts.push(layout);
}

/** -- Layout
 * @param {string} name the name of the layout
 * @param {string[]} elements an array of elements belonging to layout
 * @param {Number} index integer index number
 */
function Layout( name, elements )
{
    this._name = name;
    this._elements = elements;
}

// Getter for the layout name.
Object.defineProperty(Layout.prototype, 'getName', {
    get: function() { return this._name; }
});

Layout.prototype.hide = function()
{
    this._elements.forEach(function(element)
    {
        var s_el = "#" + element;
        document.querySelector(s_el).classList.add("hidden");
    });
}

Layout.prototype.show = function()
{
    this._elements.forEach(function(element)
    {
        var s_el = "#" + element;
        document.querySelector(s_el).classList.remove("hidden");
    });
}

// export {LayoutController};
