/** -- CanvasController
 * @version 0.2
 * @param {canvas} canv - The canvas to handle
 * @param {Element} button_left - Button to get the width and height
 */

function CanvasController( canv, movement_area, uses_mouse )
{
    this.canvas = canv;
    this.movement_area = movement_area;
    this.uses_mouse = uses_mouse;
    
    this._context = this.canvas.getContext('2d');
    this._finger = undefined; // Basically none
    this._position = undefined;
    this._directions = { "left": -1, "right": 1, "none": 0 };
}

Object.defineProperties(CanvasController.prototype, {
    lastPosition: {
        // Setter & Getter for the last known position of the finger
        set: function( value ) { this._position = value; },
        get: function()        { return this._position; }
    },
    canvasFinger: {
        // Setter & Getter for the current canvas finger
        set: function( value ) { this._finger = value; },
        get: function()        { return this._finger; }
    }
});

// Updating the canvas to fit the current button sizes.
CanvasController.prototype.updateCanvas = function()
{
    this.canvas.width = this.movement_area.getBoundingClientRect().width;
    this.canvas.height = this.movement_area.getBoundingClientRect().height;
    this._context.fillStyle = "rgba(255, 255, 255, 0.3)";
}

CanvasController.prototype.drawTouch = function( pos_x, pos_y, size )
{
    this._context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    if (pos_x !== undefined && pos_y !== undefined && size !== undefined)
    {
        this._context.beginPath();
        this._context.arc(pos_x, pos_y, size, 0, 2 * Math.PI);
        this._context.fill();
    }
}

// Returns the string of the position, returns an integer if ret_int is true
CanvasController.prototype.getCurrentPosition = function( pos_x, ret_int )
{
    // return left if true, right when false
    if (ret_int === true)
        return((pos_x / this.canvas.width) <= 0.5) ? -1 : 1;
    else
        return ((pos_x / this.canvas.width) <= 0.5) ? "left" : "right";
}

CanvasController.prototype.handleTouch = function( event, is_pressed, callback )
{   // MouseEvent: event.clientX/Y
    // TouchEvent: event.touches[id].pageX/Y
    var _x, _y, s_pos, i_pos;
    if (event === undefined || is_pressed === undefined)
        return null;

    if (is_pressed === false)
    {
        if (this.lastPosition)
        {   // Reset the current pressed button and set the last position to none
            var s_btn = "#btn-" + this.lastPosition;
            document.querySelector(s_btn).classList.remove("pressed");
            this.lastPosition = undefined;
        }
        callback("move", "direction", 0);
        if (this.canvasFinger)
            this.canvasFinger = undefined;
    }

    if ((event.type === "mousedown" || event.type === "mousemove") && event.buttons === 1)
    {   // The event uses mouse.
        _x = event.pageX;
        _y = event.pageY;
        s_pos = this.getCurrentPosition(_x);
        if (this.lastPosition === s_pos)
            return null;

        i_pos = this.getCurrentPosition(_x, true);
        callback("move", "direction", i_pos);

        if (this.lastPosition && this.lastPosition !== s_pos)
        {
            var s_btn = "#btn-" + this.lastPosition;
            document.querySelector(s_btn).classList.remove("pressed");
        }

        document.querySelector("#btn-" + s_pos).classList.add("pressed");
        this.lastPosition = s_pos;
    }
    else if ((event.type === "touchstart" || event.type === "touchmove") && event.targetTouches)
    {   // The event uses touch.
        // if (this.canvasFinger === undefined)
        //     this.canvasFinger = event.targetTouches.item(0); // return the first applicable touch

        // _x = this.canvasFinger.pageX;
        _x = event.targetTouches.item(0).pageX;
        // _y = this.canvasFinger.pageY;
        s_pos = this.getCurrentPosition(_x);
        if (this.lastPosition === s_pos)
            return null;

        i_pos = this.getCurrentPosition(_x, true);
        callback("move", "direction", i_pos);

        if (this.lastPosition && this.lastPosition !== s_pos)
        {
            var s_btn = "#btn-" + this.lastPosition;
            document.querySelector(s_btn).classList.remove("pressed");
        }

        document.querySelector("#btn-" + s_pos).classList.add("pressed");
        this.lastPosition = s_pos;
    }
}

CanvasController.prototype.disable = function()
{
    this.canvas.classList.add("hidden");
}

CanvasController.prototype.enable = function()
{
    this.canvas.classList.remove("hidden");
}


// export {CanvasController};