/** MessageController
 * @version 0.2
 * @param {string} action
 * @param {string} data
 * @param {boolean} pressed
 * 
 * @method getJSON Returns JSON message of the message
 */
// TODO: Add a rate limiter
// MessageController Constructor
function MessageController( action, data, pressed )
{
    this.action = action;
    this.data = data;
    this.pressed = pressed;
}

MessageController.prototype.getMessage = function()
{
    var message = {};
    message.action = this.action;
    message[this.data] = this.pressed;
    return message;
}

MessageController.prototype.getJSON = function( callback )
{
    var parsed_string = JSON.stringify(this.getMessage());
    callback(parsed_string);
}

// export {MessageController};