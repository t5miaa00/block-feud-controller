/** - Main script file for the controller.
 * @version 0.1.3
 */
// 'use strict';

const States = { NONE: -1, MENU: 0, YETI: 1, BLOCKMASTER: 2, ROUND: 3, END: 4 } // State handling
// const Teams =  { BLUE: 0, RED: 1, GREEN: 2, ORANGE: 3 }
const Player_colors = [ "red", "green", "purple", "yellow" ]; // The current player colors, used in changing the teams

var air_console;

var layout_controller;
var canvas_controller;
var current_state = States.NONE;
var buttons_list = [];
var current_layout = "";
var current_team = 0; // basically syncs with the Player_colors array
var is_game_master = false;
var use_other_layout = true;
var uses_mouse = false;

// Polyfill for Nodelist foreach
// Fix for edge browsers
// Shamelessly copied from stackoverflow
if (window.NodeList && !NodeList.prototype.forEach)
{
    NodeList.prototype.forEach = Array.prototype.forEach;
}

function init()
{
    air_console = new AirConsole({
        "orientation": "landscape",
        "synchronize_time": true
    });
    air_console.onCustomDeviceStateChange = function(id, data)
    {
        if (id !== AirConsole.SCREEN)
            return;

        var my_id = air_console.getDeviceId();
        var current_block_master = data.block_master; // index 0 because blockmaster is found on every slot.
        var current_game_master = data.game_master; // index 0 because gamemaster is found on every slot.
        var is_block_master = (current_block_master === my_id);
        is_game_master = (current_game_master === my_id);

        //Debugging info:
        /*/
        console.log(
            "airconsole device data:\t", 
            "\nmy_id:\t\t\t", my_id,
            // "\nmy_index:\t\t", my_index,
            "\ncurrent_block_master:\t", current_block_master,
            "\ncurrent_game_master:\t", current_game_master,
            "\nis_block_master:\t", is_block_master,
            "\nis_game_master:\t\t", is_game_master,
            "\ncolors:\t\t\t", data.device_colors
        );
        //*/

        try
        {
            if (data.device_colors[my_id] !== "none" ||
                data.device_colors[my_id] !== undefined)
                changeColor(data.device_colors[my_id]);
            
            switch(data.screen)
            {   // Handling the screen states
                case "none":
                    changeState(States.NONE);
                    break;
                case "menu":
                    changeState(States.MENU);
                    break;
                case "game":
                    if (data.device_colors[my_id])
                    {
                        if (is_block_master == true)
                            changeState(States.BLOCKMASTER);
                        else
                            changeState(States.YETI);
                    }
                    else
                        changeState(States.NONE);
                    break;
                case "round":
                    changeState(States.ROUND);
                    break;
                case "end":
                    changeState(States.END);
                    break;
            }
        }
        catch (error)
        { // Drop the error
        }
    }
    // Initializing variables
    
    layout_controller = new LayoutController();
    // console.log(layout_controller);
    
                             // layout_name, [layout_ids]
    layout_controller.addLayout("menu", ["layout-menu", "bg-color"]);
    layout_controller.addLayout("yeti", ["layout-move", "layout-yeti", "bg-color"]);
    if (document.querySelector("#layout-yeti-oth")) // if there is another yeti layout found
        layout_controller.addLayout("yeti-oth", ["layout-move", "layout-yeti-oth", "bg-color"]);
    
    layout_controller.addLayout("blockmaster", ["layout-move", "layout-blockmaster", "bg-color"]);
    layout_controller.addLayout("none", []);
    layout_controller.addLayout("end", ["layout-end", "bg-color"]);
    //     layout_controller.changeLayout("yeti");
    layout_controller.initializeLayout("menu");
    
    // Listeners for every buttongetElementById("
    document.querySelectorAll(".button").forEach(function(button)
    {
        buttons_list.push(button);
        if (button.hasAttribute("action"))
        {
            button.addEventListener(getEventListener("start"), function(ev) { handleButtonTouch(ev, true)});
            button.addEventListener(getEventListener("end"),   function(ev) { handleButtonTouch(ev, false)});
        }
    });

    canvas_controller = new CanvasController(
        document.querySelector("#mov-canv"),
        document.querySelector("#layout-move"),
        uses_mouse);

    // Setting up all of the canvas listeners
    canvas_controller.canvas.addEventListener(getEventListener("start"), function(event)
    {
        canvas_controller.handleTouch(event, true, function(action, data, pressed) 
        {
            sendInput(action, data, pressed)
        })
    });
    
    canvas_controller.canvas.addEventListener(getEventListener("move"), function(event)
    {
        canvas_controller.handleTouch(event, true, function(action, data, pressed)
        {
            sendInput(action, data, pressed)
        })
    });
    
    canvas_controller.canvas.addEventListener(getEventListener("end"), function(event)
    {
        canvas_controller.handleTouch(event, false, function(action, data, pressed)
        {
            sendInput(action, data, pressed)
        })
    });

    canvas_controller.usesMouse = true;
    canvas_controller.disable() // This is to hide the canvas before the game is started

    changeState(States.MENU); // The initial layout
}

function changeColor( player_color )
{
    document.querySelector("svg").classList.remove("purple", "green", "red", "yellow");
    document.querySelector("svg").classList.add(player_color);
    current_team = Player_colors.indexOf(player_color);
}

function handleButtonTouch( event, is_pressed )
{
    var action = event.currentTarget.getAttribute("action");
    if (is_pressed)
    {
        event.currentTarget.classList.add("pressed");
    }
    else
    {
        event.currentTarget.classList.remove("pressed");
    }
    sendInput(action, "perform", (is_pressed) ? "yes" : "no");
}

function sendInput( action, data, pressed )
{
    //TODO: Make a message limiter
    var message = new MessageController(action, data, pressed);
    // air_console.vibrate(100);
    
    air_console.message(AirConsole.SCREEN, message.getMessage());
}

function changeState( state )
{
    // if (state === current_state) { return } // don't do anything if the state is the current state.
    
    // TODO: Clean up the previous state...
    // Set up the new state
    switch (state)
    {
        case States.NONE:
            layout_controller.changeLayout("none");
            break;
        case States.MENU:
            layout_controller.changeLayout((is_game_master) ? "menu" : "none");
            canvas_controller.disable()
            break;
        case States.YETI:
            if (use_other_layout)
            {
                layout_controller.changeLayout("yeti-oth");
            }
            else
            {
                layout_controller.changeLayout("yeti");
            }
            canvas_controller.updateCanvas();
            canvas_controller.enable();
            break;
        case States.BLOCKMASTER:
            layout_controller.changeLayout("blockmaster");
            canvas_controller.updateCanvas();
            canvas_controller.enable();
            break;
        case States.ROUND:
            layout_controller.changeLayout("none");
            canvas_controller.disable();
            break;
        case States.END:
            layout_controller.changeLayout((is_game_master) ? "end" : "none");
            canvas_controller.disable();
            break;
        default:
            console.error("State "+ state +" not found!");
            break;
    }
    current_state = state;
}

function getEventListener( suffix )
{
    if ("ontouchstart" in document.documentElement)
    {
        return "touch" + suffix;
    }
    else
    {
        uses_mouse = true;
        suffixes = {"start": "mousedown", "end": "mouseup", "move": "mousemove"};
        return suffixes[suffix];
    }
}

window.onload = init; // Start this script when the page is finished loading
